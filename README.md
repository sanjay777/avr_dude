avrdude-gui v0.2.0
========================

Copyright (c) 2004 three_dot(at)gmx(dot)net
This software is covered by the GPL.


Purpose
-------

	This tool is a GUI for avrdude (http://savannah.nongnu.org/projects/avrdude) 
	only.


Requirements
------------

	Avrdude is installed on your computer.
	The wxWidget toolkit version 2.5.x is needed to build avrdude-gui.


Installation
------------

	This program has no installation or deinstallation routines.
	Just copy this tool into the directory where avrdude is located.


Usage
-----

	...


Things to do
------------

	- Read fuses
	- Separate fuses dialog to set single fuses by a checkbox
	- Statusbar for some infos
	
