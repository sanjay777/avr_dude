#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <winbase.h>

#define COM1 4
#define SETTINGS ( _COM_9600 | _COM_NOPARITY | _COM_STOP1 | _COM_CHR8)

int main (void)
 {
 DCB dcbCommPort, dcbSerialParams; //structure Device Control Block
 COMMTIMEOUTS CommTimeouts;
 //COMMCONFIG
 //CHAR *words, *buffRead, *buffWrite;
 CHAR *words, *buffWrite;
 DWORD dwBytesWritten, dwBytesRead;
 CHAR BytesWritten, BytesRead;
 CHAR InBuffer[100];
 HANDLE hComm;
 FILE * fp;
 BOOL a;
 DWORD nwrite;

 hComm = CreateFile( "COM3",
 GENERIC_READ | GENERIC_WRITE,
 0,
 0,
 OPEN_EXISTING,
 0,
 0);

if (hComm == INVALID_HANDLE_VALUE)

 /*
 if (GetLastError() == INVALID_HANDLE_VALUE)
        {
        printf(" serial port does not exist \n");
        }
  */

 {
 printf("Could not open port.\n");
 return(1);
 }

 /*
 Write Operation*********************
 words = "This is a string to be written to serial port COM1";
 nwrite = strlen(words);
 buffWrite = words;
 dwBytesWritten = 0;

if (!WriteFile(hSerial, buffWrite, nwrite, &dwBytesWritten, NULL))
{
printf("error writing to output buffer \n");
 }
 //printf("Data written to write buffer is \n %s \n",buffWrite);


//***************Read Operation******************
 //buffRead = 0;
 dwBytesRead = 0;
 nread = strlen(words);

if (!ReadFile(hSerial, buffRead, nread, &dwBytesRead, NULL))
{
 printf("error reading from input buffer \n");
 }
 printf("Data read from read buffer is \n %s \n",buffRead);

CloseHandle(hSerial);
}
*/

// & is the reference operator and can be read as "address of"
// * is the dereference operator and can be read as "value pointed by"

/*
if(!BuildCommDCB(BAUD_115200 PARITY_NONE DATABITS_8 STOPBITS_10, &dcbCommPort))
		 {
		 hComm = NULL;
         printf("Cannot build comm DCB.\n");
		 return(1);
		 }
*/
 dcbCommPort.BaudRate=CBR_115200;
 dcbCommPort.ByteSize = 8;
 dcbCommPort.StopBits = ONESTOPBIT;
 dcbCommPort.Parity = NOPARITY;
 dcbCommPort.fBinary = TRUE;
 dcbCommPort.fDtrControl = DTR_CONTROL_DISABLE;
 dcbCommPort.fRtsControl = RTS_CONTROL_DISABLE;
 dcbCommPort.fOutxCtsFlow = FALSE;
 dcbCommPort.fOutxDsrFlow = FALSE;
 dcbCommPort.fDsrSensitivity= FALSE;
 dcbCommPort.fAbortOnError = TRUE;

a=GetCommState(hComm, &dcbCommPort);

if (!a)
    {
    printf("error setting serial port state \n");
    }

if (!SetCommState(hComm, &dcbCommPort))
    {
    printf("ComState is not set");
    exit(1);
    }

if(!SetCommState(hComm, &dcbCommPort))
		 {
		 CloseHandle(hComm);
		 hComm = NULL;
		 printf("Cannot set comm state.\n");
		 return(1);
		 }

 CommTimeouts.ReadIntervalTimeout = MAXDWORD;
 CommTimeouts.ReadTotalTimeoutConstant = 500; //5
 CommTimeouts.ReadTotalTimeoutMultiplier = 500; //5
 CommTimeouts.WriteTotalTimeoutConstant = 500; //250
 CommTimeouts.WriteTotalTimeoutMultiplier = 500; //1

if(!SetCommTimeouts(hComm, &CommTimeouts))
		 {
		 CloseHandle(hComm);
		 hComm = NULL;
		 printf("Cannot set comm timeouts.\n");
		 return;
		 }

int CommFlag = 1;
int i;
DWORD *words1;
fp = fopen("test.txt","w");

//for (i=0;i<5;i++)
while(CommFlag==1)
{
printf("input a character \n");
scanf("%s", &words1); //%d for number like integer, %f for float, %e for scietific, %c for character, %s for string.
//printf("%c \n", words1);
nwrite = strlen(&words); //SR 1Jan2013
buffWrite = &words1;
dwBytesWritten = 0;

if (!WriteFile(hComm, buffWrite, 1, &dwBytesWritten, NULL))
 {
 printf("error writing to output buffer \n");
 }
printf("Data written to write buffer is \n%s \n",buffWrite);
Sleep(1);
//printf("would you like to quit? \nPRESS ZERO TO QUIT!\n");
//scanf("%d", &CommFlag);
fprintf(fp,"%s\n", &words1);

//ReadFile(hComm, InBuffer, 1, &BytesRead, NULL);

	/*	if(BytesRead)
			{
			 InBuffer[BytesRead]=0;
			 fprintf(fp,"%s\n", InBuffer);
			 printf(" %s",InBuffer);
			 fclose(fp);
			}
*/
}
fclose(fp);

//return(0);

/*
//WRITE
words = "1 2 3 4"; //SR 1Jan2013
nwrite = strlen(words); //SR 1Jan2013
buffWrite = words; //SR 1Jan2013
dwBytesWritten = 0;

if (!WriteFile(hComm, buffWrite, nwrite, &dwBytesWritten, NULL))
 {
 printf("error writing to output buffer \n");
 }

printf("Data written to write buffer is \n %s \n",buffWrite);

//READ
while(CommFlag)
	{
		Sleep(3000); //THIS USUALLY COMES OUT TO BE 3 CURSOR CLICKS ON THE OUTPUT WINDOW.
		ReadFile(hComm, InBuffer, nwrite, &BytesRead, NULL);

		if(BytesRead)
			{
			 InBuffer[BytesRead]=0;
			 fprintf(fp,"%s\n", InBuffer);
			 printf(" %s",InBuffer);
			 fclose(fp);
			}
	}

*/

 exit(0);
 }
